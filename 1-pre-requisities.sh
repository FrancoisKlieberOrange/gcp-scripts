#!/bin/bash

if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_REGION,GCP_ZONE,GCP_PROJECT,CLUSTER_NAME,GCP_NETWORK_NAME,GCP_SUBNETWORK_NAME,GCP_SUBNETWORK_CIDR"


gcloud config set account ${GCP_ACCOUNT}
gcloud config set project ${GCP_PROJECT}

gcloud services enable compute.googleapis.com 
gcloud services enable iam.googleapis.com 
gcloud services enable container.googleapis.com

# create network default
gcloud compute networks create ${GCP_NETWORK_NAME} --project=${GCP_PROJECT} --subnet-mode=custom --mtu=1500 --bgp-routing-mode=regional

gcloud compute networks subnets create ${GCP_SUBNETWORK_NAME} --project=${GCP_PROJECT} --range=${GCP_SUBNETWORK_CIDR} --stack-type=IPV4_ONLY --network=${GCP_NETWORK_NAME} --region=${GCP_REGION}

gcloud compute firewall-rules create default-allow-custom --project=${GCP_PROJECT} --network=projects/${GCP_PROJECT}/global/networks/${GCP_NETWORK_NAME} --description=Autorise\ la\ connexion\ \à\ partir\ de\ toutes\ les\ sources\ sur\ toutes\ les\ instances\ du\ r\éseau\ utilisant\ des\ protocoles\ personnalis\és. --direction=INGRESS --priority=65534 --action=ALLOW --rules=all

gcloud compute firewall-rules create default-allow-icmp --project=${GCP_PROJECT} --network=projects/${GCP_PROJECT}/global/networks/${GCP_NETWORK_NAME} --description=Autorise\ les\ connexions\ ICMP\ \à\ partir\ de\ toutes\ les\ sources\ sur\ toutes\ les\ instances\ du\ r\éseau. --direction=INGRESS --priority=65534 --source-ranges=0.0.0.0/0 --action=ALLOW --rules=icmp

gcloud compute firewall-rules create default-allow-rdp --project=${GCP_PROJECT} --network=projects/${GCP_PROJECT}/global/networks/${GCP_NETWORK_NAME} --description=Autorise\ les\ connexions\ RDP\ \à\ partir\ de\ toutes\ les\ sources\ sur\ toutes\ les\ instances\ du\ r\éseau\ utilisant\ le\ port\ 3389. --direction=INGRESS --priority=65534 --source-ranges=0.0.0.0/0 --action=ALLOW --rules=tcp:3389

gcloud compute firewall-rules create default-allow-ssh --project=${GCP_PROJECT} --network=projects/${GCP_PROJECT}/global/networks/${GCP_NETWORK_NAME} --description=Autorise\ les\ connexions\ TCP\ \à\ partir\ de\ toutes\ les\ sources\ sur\ toutes\ les\ instances\ du\ r\éseau\ utilisant\ le\ port\ 22. --direction=INGRESS --priority=65534 --source-ranges=0.0.0.0/0 --action=ALLOW --rules=tcp:22

# Ensure if network list contains default network
gcloud compute networks list --project="${GCP_PROJECT}"

gcloud compute networks describe "${GCP_NETWORK_NAME}" --project="${GCP_PROJECT}"

# Ensure if firewall rules are enabled
gcloud compute firewall-rules list --project "$GCP_PROJECT"

# Create routers
gcloud compute routers create "${CLUSTER_NAME}-myrouter" --project="${GCP_PROJECT}" --region="${GCP_REGION}" --network="${GCP_NETWORK_NAME}"

# Create NAT
gcloud compute routers nats create "${CLUSTER_NAME}-mynat" --project="${GCP_PROJECT}" --router-region="${GCP_REGION}" --router="${CLUSTER_NAME}-myrouter" --nat-all-subnet-ip-ranges --auto-allocate-nat-external-ips
