#!/bin/bash


if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_ZONE,GCP_PROJECT,GCP_SUBNETWORK_NAME,GCP_PACKER_MACHINE_TYPE,GCP_BOOTSTRAP_MACHINE_TYPE,GCP_SA_IMAGE_BUILDER_NAME"

create_gcp_service_account () {
# create a service account for image
gcloud iam service-accounts create ${GCP_SA_IMAGE_BUILDER_NAME} \
    --description="build image for kubeadm" \
    --display-name="${GCP_SA_IMAGE_BUILDER_NAME}"

gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
    --member="serviceAccount:${GCP_SA_IMAGE_BUILDER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
    --role="roles/editor"

# create key for SA
gcloud iam service-accounts keys create ${GCP_SA_IMAGE_BUILDER_NAME}.json \
    --iam-account=${GCP_SA_IMAGE_BUILDER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com
}


check_vm_created() {
if 
    gcloud compute instances describe bootstrap-vm --project=${GCP_PROJECT} --zone=${GCP_ZONE}
then
    echo "VM exist"
else
    gcloud compute instances create bootstrap-vm \
        --project=$GCP_PROJECT \
        --zone=$GCP_ZONE \
        --machine-type=$GCP_BOOTSTRAP_MACHINE_TYPE \
        --network-interface=stack-type=IPV4_ONLY,subnet=$GCP_SUBNETWORK_NAME,no-address \
        --metadata=enable-oslogin=true \
        --maintenance-policy=MIGRATE \
        --provisioning-model=STANDARD \
        --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
        --create-disk=auto-delete=yes,boot=yes,device-name=bootstrap-vm,image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240208,mode=rw,size=30,type=projects/${GCP_PROJECT}/zones/$GCP_ZONE/diskTypes/pd-balanced \
        --no-shielded-secure-boot \
        --shielded-vtpm \
        --shielded-integrity-monitoring \
        --labels=goog-ec-src=vm_add-gcloud \
        --reservation-affinity=any
fi
}

init_vm () {
echo "Initializing VM"

filename="${GCP_SA_IMAGE_BUILDER_NAME}.json"

encoded_sa_json=$(base64 $filename)


gcloud compute ssh --zone $GCP_ZONE "bootstrap-vm" --project $GCP_PROJECT -- "echo '$encoded_sa_json' | base64 --decode > sa.json"
gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
if [ ! -d '/opt/image-builder' ]; then \
    sudo mkdir /opt/image-builder; \
    sudo chown -R \$USER: /opt/image-builder; \
    git clone https://github.com/kubernetes-sigs/image-builder.git /opt/image-builder; \
    sudo apt-get update && sudo apt-get install -y make python3 curl python3-distutils python3-pip unzip jq; \
else \
    echo 'Image-builder already cloned'; \
fi"


echo "Initialized VM"
}


update_image_builder () {
  echo "Update image builder file"

  encoded_UBUNTU=$(envsubst < "./templates/ubuntu-2204.json" | base64)
  encoded_PACKER=$(envsubst < "./templates/packer.json" | base64)


  gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
  echo '$encoded_UBUNTU' | base64 --decode > /opt/image-builder/images/capi/packer/gce/ubuntu-2204.json; \
  echo '$encoded_PACKER' | base64 --decode > /opt/image-builder/images/capi/packer/gce/packer.json"

}

make_build () {
  echo "building image"

  gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
  export GOOGLE_APPLICATION_CREDENTIALS="\$HOME/sa.json"; \
  export GCP_PROJECT_ID="$GCP_PROJECT"; \
  export PATH=\$HOME/.local/bin:/opt/image-builder/images/capi/.local/bin:\$PATH; \
  cd /opt/image-builder/images/capi; \
  make build-gce-ubuntu-2204"
}

###########

# commands are applied here

###########

filename="${GCP_SA_IMAGE_BUILDER_NAME}.json"

if [ -f $filename ]; then
    echo 'File exists.'
else
    echo 'File does not exist.'
    create_gcp_service_account
fi

check_vm_created

wait_ssh_available

init_vm

update_image_builder

make_build