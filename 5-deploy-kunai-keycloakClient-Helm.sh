#!/bin/bash

read -e -p "Enter the GITLAB_TOKEN for registry.gitlab.com that have access to kunai package: " -i "${GITLAB_TOKEN:-Need_to_be_defined}" GITLAB_TOKEN
    export GITLAB_TOKEN=$GITLAB_TOKEN

if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_ZONE,GCP_PROJECT,GCP_SUBNETWORK_NAME,GITLAB_TOKEN"


deploy_kunai_keycloak_client () {

    
    encoded_KEYCLOAK_CLIENT=$(envsubst < "./templates/keycloak-client-kunai.yaml" | base64)
    

    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    echo '$encoded_KEYCLOAK_CLIENT' | base64 --decode > ~/keycloak-client-kunai.yaml; \
    kubectl apply -f ~/keycloak-client-kunai.yaml;"
}

deploy_kunai_helm_release () {
    sleep 20

    get_secret=$(echo "export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig;kubectl get secrets sylva-ca.crt -n vault -o jsonpath=\"{.data['ca\.crt']}\"| base64 -d"| base64 )

    NEXTAUTH_SECRET=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)
    export NEXTAUTH_SECRET=$NEXTAUTH_SECRET

    SYLVA_CA=$(gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    mkdir -p /opt/sylva-core/environment-values/current/; \
    echo '$get_secret' | base64 --decode > ~/get_sylva_ca.sh; \
    sudo chmod +x ~/get_sylva_ca.sh; \
    ~/get_sylva_ca.sh;")
    export SYLVA_CA=$SYLVA_CA

    KUNAI_KEYCLOAK_CLIENT_SECRET=$(gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    kubectl get secret keycloak-client-secret-kunai-client -n keycloak -o jsonpath={.data.CLIENT_SECRET} | base64 -d")
    export KUNAI_KEYCLOAK_CLIENT_SECRET=$KUNAI_KEYCLOAK_CLIENT_SECRET

    export placeholder="||"
    export SYLVA_CA=$(echo "$SYLVA_CA" | sed "s/\\\\n/${placeholder}/g")
    export SYLVA_CA=$(echo "$SYLVA_CA" | sed 's/^/          /')
    encoded_HELM_RELEASE=$(envsubst < "./templates/kunai-helm-release.yaml" | sed "s/$placeholder/\\\\n/g" | base64)
    
    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    echo '$encoded_HELM_RELEASE' | base64 --decode > ~/kunai-helm-release.yaml; \
    kubectl apply -f ~/kunai-helm-release.yaml;"
}

deploy_kunai-dev_helm_release () {

    get_secret=$(echo "export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig;kubectl get secrets sylva-ca.crt -n vault -o jsonpath=\"{.data['ca\.crt']}\"| base64 -d"| base64 )

    NEXTAUTH_SECRET=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | head -c 10)
    export NEXTAUTH_SECRET=$NEXTAUTH_SECRET

    SYLVA_CA=$(gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    mkdir -p /opt/sylva-core/environment-values/current/; \
    echo '$get_secret' | base64 --decode > ~/get_sylva_ca.sh; \
    sudo chmod +x ~/get_sylva_ca.sh; \
    ~/get_sylva_ca.sh;")
    export SYLVA_CA=$SYLVA_CA

    KUNAI_KEYCLOAK_CLIENT_SECRET=$(gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    kubectl get secret keycloak-client-secret-kunai-client -n keycloak -o jsonpath={.data.CLIENT_SECRET} | base64 -d")
    export KUNAI_KEYCLOAK_CLIENT_SECRET=$KUNAI_KEYCLOAK_CLIENT_SECRET

    export placeholder="||"
    export SYLVA_CA=$(echo "$SYLVA_CA" | sed "s/\\\\n/${placeholder}/g")
    export SYLVA_CA=$(echo "$SYLVA_CA" | sed 's/^/          /')
    encoded_HELM_RELEASE=$(envsubst < "./templates/kunai-dev-helm-release.yaml" | sed "s/$placeholder/\\\\n/g" | base64)
    
    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    echo '$encoded_HELM_RELEASE' | base64 --decode > ~/kunai-dev-helm-release.yaml; \
    kubectl apply -f ~/kunai-dev-helm-release.yaml;"
}

###########

# commands are applied here

###########

deploy_kunai_keycloak_client
deploy_kunai_helm_release
deploy_kunai-dev_helm_release