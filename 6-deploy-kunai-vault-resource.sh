#!/bin/bash


if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_ZONE,GCP_PROJECT,GCP_SUBNETWORK_NAME,"


deploy_vault_kunai_secret_engine () {

    
    encoded_VAULT=$(envsubst < "./templates/vault-kunai-secret-engine.yaml" | base64)
    

    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    echo '$encoded_VAULT' | base64 --decode > ~/vault-kunai-secret-engine.yaml; \
    kubectl apply -f ~/vault-kunai-secret-engine.yaml;"
}

###########

# commands are applied here

###########

deploy_vault_kunai_secret_engine
