#!/bin/bash

# defined Project Name 
read -e -p "Enter Your GCP_PROJECT: " -i "${GCP_PROJECT:-Need_to_be_defined}" GCP_PROJECT
export GCP_PROJECT=$GCP_PROJECT
read -e -p "Enter Your GCP_ACCOUNT: " -i "${GCP_ACCOUNT:-Need_to_be_defined}" GCP_ACCOUNT
export GCP_ACCOUNT=$GCP_ACCOUNT

read -e -p "Enter Your SSH_PUBLIC_KEY_UBUNTU: " -i "${SSH_PUBLIC_KEY_UBUNTU:-ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQD0QbbCEqciiCSgx2dhb2pyVr5qNU3p3jCXcKKN4h3YEB7NbrkC+K+AGP2JZNwrtEVXbSIfc10/c+Rhax4ZH0ZWkPHwu2MV1NHxp9pj5rrUqqsaqJE8YtUdeRzvMsU/8/QY+AYpS4Seu6gTCg9MKtEx/YOGHC5LYbqx/ueC6kF4ZbZ9qrLUmSa7erhxvvRkkdWEtG9stTInM7+aJHS6RuVTJ9lORXr4z/RUbM1zsK7DeKzD3+iOxwBjTM4lQRiE2+mAKALGX4FNpuAy9v+PPtNf0mMpGpuT+32sVM3/igSSaUzUes4qFrmCTgrr2x/72cUOewg6TiPoW5KhdvPnjIG/}" SSH_PUBLIC_KEY_UBUNTU
export SSH_PUBLIC_KEY_UBUNTU=$SSH_PUBLIC_KEY_UBUNTU


# define Project defaults zone is Paris
read -e -p "Enter Your GCP_REGION: " -i "${GCP_REGION:-europe-west9}" GCP_REGION
export GCP_REGION=$GCP_REGION
read -e -p "Enter Your GCP_ZONE: " -i "${GCP_ZONE:-europe-west9-a}" GCP_ZONE
export GCP_ZONE=$GCP_ZONE

read -e -p "Enter Your CLUSTER_NAME: " -i "${CLUSTER_NAME:-test}" CLUSTER_NAME
export CLUSTER_NAME=$CLUSTER_NAME

# define machine type used for each steps
read -e -p "Enter Your GCP_BOOTSTRAP_MACHINE_TYPE: " -i "${GCP_BOOTSTRAP_MACHINE_TYPE:-e2-standard-8}" GCP_BOOTSTRAP_MACHINE_TYPE
export GCP_BOOTSTRAP_MACHINE_TYPE=$GCP_BOOTSTRAP_MACHINE_TYPE
read -e -p "Enter Your GCP_PACKER_MACHINE_TYPE: " -i "${GCP_PACKER_MACHINE_TYPE:-e2-standard-8}" GCP_PACKER_MACHINE_TYPE
export GCP_PACKER_MACHINE_TYPE=$GCP_PACKER_MACHINE_TYPE
read -e -p "Enter Your GCP_CLUSTER_MACHINE_TYPE: " -i "${GCP_CLUSTER_MACHINE_TYPE:-e2-standard-4}" GCP_CLUSTER_MACHINE_TYPE
export GCP_CLUSTER_MACHINE_TYPE=$GCP_CLUSTER_MACHINE_TYPE

# Define GCP network
read -e -p "Enter Your GCP_NETWORK_NAME: " -i "${GCP_NETWORK_NAME:-default}" GCP_NETWORK_NAME
export GCP_NETWORK_NAME=$GCP_NETWORK_NAME
read -e -p "Enter Your GCP_SUBNETWORK_NAME: " -i "${GCP_SUBNETWORK_NAME:-test}" GCP_SUBNETWORK_NAME
export GCP_SUBNETWORK_NAME=$GCP_SUBNETWORK_NAME
read -e -p "Enter Your GCP_SUBNETWORK_CIDR: " -i "${GCP_SUBNETWORK_CIDR:-10.200.0.0/20}" GCP_SUBNETWORK_CIDR
export GCP_SUBNETWORK_CIDR=$GCP_SUBNETWORK_CIDR


read -e -p "Enter Your GCP_SA_MANAGEMENT_CLUSTER_NAME: " -i "${GCP_SA_MANAGEMENT_CLUSTER_NAME:-capi-management}" GCP_SA_MANAGEMENT_CLUSTER_NAME
export GCP_SA_MANAGEMENT_CLUSTER_NAME=$GCP_SA_MANAGEMENT_CLUSTER_NAME
read -e -p "Enter Your GCP_SA_IMAGE_BUILDER_NAME: " -i "${GCP_SA_IMAGE_BUILDER_NAME:-sa-builder}" GCP_SA_IMAGE_BUILDER_NAME
export GCP_SA_IMAGE_BUILDER_NAME=$GCP_SA_IMAGE_BUILDER_NAME

read -e -p "Enter Your GIT_SYLVA_CORE: " -i "${GIT_SYLVA_CORE:-https://gitlab.com/FrancoisKlieberOrange/sylva-core}" GIT_SYLVA_CORE
export GIT_SYLVA_CORE=$GIT_SYLVA_CORE
read -e -p "Enter Your GIT_SYLVA_CORE_BRANCH: " -i "${GIT_SYLVA_CORE_BRANCH:-feature/add-gce}" GIT_SYLVA_CORE_BRANCH
export GIT_SYLVA_CORE_BRANCH=$GIT_SYLVA_CORE_BRANCH


# Fonction pour vérifier la présence de variables d'environnement
verifier_variables() {
  # Convertir la liste de noms de variables en tableau
  IFS=',' read -r -a variables <<< "$1"

  # Boucler sur chaque variable pour vérifier si elle est définie
  for variable in "${variables[@]}"; do
    if [[ -z "${!variable}" ]]; then
      echo "Erreur: La variable d'environnement '$variable' n'est pas définie."
      return 1
    fi
  done

  echo "Toutes les variables sont correctement définies."
  return 0
}


execute_ssh_command() {
    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "echo 'SSH ready'"
}

wait_ssh_available () {
    # Maximum number of retries
    max_retries=10
    # Initial retry count
    retry_count=0

    # Initial attempt
    execute_ssh_command
    # Loop until the SSH command succeeds or maximum retries reached
    while [ $? -ne 0 ] && [ $retry_count -lt $max_retries ]; do
        echo "SSH command failed, retrying in 5 seconds..."
        sleep 5
        retry_count=$((retry_count+1))
        execute_ssh_command
    done

}
