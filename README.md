---
title: Create management cluster using sylva OTC tools on GCP
description: Tutorial to create sylva management cluster on GCP. Then we create sylva workload cluster using Kunai.
---

# Objectives

We are going to create a management cluster on GCP on a [sandbox project provided by orange](sandbox.cloud.orange/)

The management cluster will look like this in GCP console
![workshop_gcp.jpg](./workshop_gcp.jpg)

Once installed, we are going to deploy kunai. Kunai is an application that enable easy management of sylva workload cluster.

We are then going to deploy a workload cluster.

## Prerequisites

- [Create a GCP sandbox project](https://sandbox.cloud.orange/)
- [Install gcloud CLI](https://cloud.google.com/sdk/docs/install?hl=fr)
- Dispose a linux environment Ubuntu VM or WSL


## Tutorial

### Allow Service Account Key generation

Enable following managed policies: 
 - Allow Service Account Key generation.
 - Allow IP Forwarding.

![Allow_managed_policies.PNG](./Allow_managed_policies.PNG)



### Login in GCP with the CLI

If you are on windows, please do it on both windows and WSL.

```bash
gcloud auth login ACCOUNT NAME
```

### Accept Cloud services terms

[Cloud service terms](https://console.cloud.google.com/terms/cloud)


### Set variables
```bash
. ./0-init-variable.sh 
```

### Initialize the GCP sandbox project
```bash
./1-pre-requisities.sh 
```

### Optionnal create an image of ubuntu with kubeadm in version 1.27.10 in our project. Usefull for airgapped deployment.
```bash
./2-create-image-build.sh
```

### Launch the Sylva management cluster. 

This step is deploying a Sylva management cluster on GCP using Google Cloud compute VM. The kubernetes cluster will be rke2 1.27.10.


```bash
./3-launch-sylva-management.sh
```

### Create a private DNS for sylva in GCP.
```bash
./4-create-private-dns-sylva.sh
```
### Deploy kunai
#### Deploy the application
```bash
./5-deploy-kunai-keycloakClient-Helm
```

#### Setup Vault for kunai-platform-secret


```bash
./6-deploy-kunai-vault-resource
```

### deploy a workload cluster

#### Create an empty git repository on Gitlab.

![gitlab_create_new_repo_1.PNG](./gitlab_create_new_repo_1.PNG)
![gitlab_create_new_repo_2.PNG](./gitlab_create_new_repo_2.PNG)
Update the name and location to your need
![gitlab_create_new_repo_3.PNG](./gitlab_create_new_repo_3.PNG)

Create an access token with role "maintainer" and scope "api, read_repository, write_repository".
![gitlab_access_token.PNG](./gitlab_access_token.PNG)
![gitlab_access_token_2.PNG](./gitlab_access_token_2.PNG)
![gitlab_access_token_3.PNG](./gitlab_access_token_3.PNG)


#### Retrieve a rancher kubeconfig.

```bash
gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    kubectl get secret credential-sylva-sylva-admin-keycloak -n keycloak -o jsonpath={.data.password} | base64 -d"
```

[Visite Rancher url](https://rancher.sylva/) and login using keycload and sylva-admin user

Select local cluster
![rancher_local_cluster.PNG](./rancher_local_cluster.PNG)
Retrieve kubeconfig
![rancher_download_kubeconfig.PNG](./rancher_download_kubeconfig.PNG)
Get kubeconfig token and server url
![rancher_info.PNG](./rancher_info.PNG)

#### Create a Kunai project.

[Visite Kunai url](https://kunai.sylva/)

![kunai_create_project_1.PNG](./kunai_create_project_1.PNG)
![kunai_create_project_2.PNG](./kunai_create_project_2.PNG)

#### Create a workload cluster

##### Add a template to kunai catalogue
![kunai_add_workload.PNG](./kunai_add_workload.PNG)
![kunai_add_template.PNG](./kunai_add_template.PNG)

- repository: https://gitlab.com/FrancoisKlieberOrange/gcp-dev
- tag: main
- token: glpat-PeFW--rT8YhcQ5EwWcMX


##### Add secret to vault

[Visite Vault url](https://vault.sylva/) using "kunai" role
![login_vault_kunai.PNG](./login_vault_kunai.PNG)


Select kunai-platform-secrets
![vault_kunai-plateform-secrets.PNG](./vault_kunai-plateform-secrets.PNG)
![vault_kunai-plateform-secrets_create.PNG](./vault_kunai-plateform-secrets_create.PNG)
![vault_kunai-plateform-secrets_create_entry.PNG](./vault_kunai-plateform-secrets_create_entry.PNG)

path: gcp/project
key: credentials
content: 
```yaml
---
cluster:
  capg:
    project: GCP_PROJECT
    credentials: |
      GCP_SA_JSON
    machineSpec:
      serviceAccounts:
        email: GCP_SA_EMAIL
```

##### Deploy cluster
![deploy-workload.PNG](./deploy-workload.PNG)



# Bonus VSCode remote SSH

Open a tunnel to the Bootstrap VM with the following script.
```bash
gcloud compute ssh --ssh-flag="-L 2222:localhost:22"  --ssh-flag="-D 20005" --zone europe-west9-a "bootstrap-vm" --project sbx-31371-11lxr3pzwb08t8alrl1t
```

update your SSH config

```text
Host gcp-boostrap
   HostName localhost
   Port 2222
   user adrian_hane_1709648556228_sandbo
   IdentityFile ~\.ssh\google_compute_engine
```

Use VSCode SSH remote explorer.
