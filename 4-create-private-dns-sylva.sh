#!/bin/bash

if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_ZONE,GCP_PROJECT,GCP_SUBNETWORK_NAME"



create_dns_private_zone() {

    external_ip=$(gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    export KUBECONFIG=/opt/sylva-core/management-cluster-kubeconfig; \
    kubectl get svc rke2-ingress-nginx-controller -n kube-system -o jsonpath={.status.loadBalancer.ingress[0].ip};")

    echo "$external_ip"

    echo "enable API"
    gcloud services enable dns.googleapis.com 

    echo "create zone"
    gcloud dns --project=${GCP_PROJECT} managed-zones create sylva --description="" --dns-name="sylva." --visibility="private" --networks="${GCP_NETWORK_NAME}"

    

    echo "create entry"


    gcloud dns --project=${GCP_PROJECT} record-sets create *.sylva. --zone="sylva" --type="A" --ttl="300" --rrdatas="$external_ip"
}


###########

# commands are applied here

###########


create_dns_private_zone
