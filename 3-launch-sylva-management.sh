#!/bin/bash
if ! declare -f verifier_variables > /dev/null; then
  source ./0-init-variable.sh
  
  # Vérifier à nouveau si la fonction est maintenant disponible
  if ! declare -f verifier_variables > /dev/null; then
    echo "Erreur: Impossible de charger 'verifier_variables' depuis ./0-init-variable.sh"
    exit 1
  fi
fi

# Exemple d'utilisation de la fonction
# Remplacer var1,var2,var3 par les noms des variables d'environnement à vérifier
verifier_variables "GCP_ZONE,GCP_PROJECT,GCP_SUBNETWORK_NAME,GCP_CLUSTER_MACHINE_TYPE,GCP_BOOTSTRAP_MACHINE_TYPE,GCP_SA_MANAGEMENT_CLUSTER_NAME"


create_gcp_service_account () {
    filename="${GCP_SA_MANAGEMENT_CLUSTER_NAME}.json"

    if [ -f $filename ]; then
        echo 'File exists.'
    else
        echo 'File does not exist.'
        # create a service account for image
        gcloud iam service-accounts create ${GCP_SA_MANAGEMENT_CLUSTER_NAME} \
            --description="build image for kubeadm" \
            --display-name="${GCP_SA_MANAGEMENT_CLUSTER_NAME}"

        gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
            --member="serviceAccount:${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
            --role="roles/editor"
        gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
            --member="serviceAccount:${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
            --role="roles/iam.serviceAccountUser"

        gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
            --member="serviceAccount:${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
            --role="roles/iam.serviceAccountTokenCreator"

        gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
            --member="serviceAccount:${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
            --role="roles/iam.serviceAccountViewer"

        gcloud projects add-iam-policy-binding ${GCP_PROJECT} \
            --member="serviceAccount:${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com" \
            --role="roles/container.admin"


        # create key for SA
        gcloud iam service-accounts keys create ${GCP_SA_MANAGEMENT_CLUSTER_NAME}.json \
            --iam-account=${GCP_SA_MANAGEMENT_CLUSTER_NAME}@${GCP_PROJECT}.iam.gserviceaccount.com
    fi

    
}

create_bootstrap_vm() {
    if 
        gcloud compute instances describe bootstrap-vm --project=${GCP_PROJECT} --zone=${GCP_ZONE} > /dev/null
    then
        echo "VM exist"
    else
        gcloud compute instances create bootstrap-vm \
            --project=$GCP_PROJECT \
            --zone=$GCP_ZONE \
            --machine-type=$GCP_BOOTSTRAP_MACHINE_TYPE \
            --network-interface=stack-type=IPV4_ONLY,subnet=$GCP_SUBNETWORK_NAME,no-address \
            --metadata=enable-oslogin=true \
            --maintenance-policy=MIGRATE \
            --provisioning-model=STANDARD \
            --scopes=https://www.googleapis.com/auth/devstorage.read_only,https://www.googleapis.com/auth/logging.write,https://www.googleapis.com/auth/monitoring.write,https://www.googleapis.com/auth/servicecontrol,https://www.googleapis.com/auth/service.management.readonly,https://www.googleapis.com/auth/trace.append \
            --create-disk=auto-delete=yes,boot=yes,device-name=bootstrap-vm,image=projects/ubuntu-os-cloud/global/images/ubuntu-2204-jammy-v20240208,mode=rw,size=30,type=projects/${GCP_PROJECT}/zones/$GCP_ZONE/diskTypes/pd-balanced \
            --no-shielded-secure-boot \
            --shielded-vtpm \
            --shielded-integrity-monitoring \
            --labels=goog-ec-src=vm_add-gcloud \
            --reservation-affinity=any
    fi
}

create_ubuntu_user_and_group () {
    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    sudo groupadd -f ubuntu
    sudo usermod -md /home/ubuntu -u 1001 -c ubuntu -G adm,sudo,docker,ubuntu -l ubuntu --shell /bin/bash ubuntu && sudo groupmod -g 1001 -n docker
    echo 'ubuntu ALL=(ALL) NOPASSWD:ALL' | sudo tee /etc/sudoers.d/ubuntu
    sudo mkdir -p /home/ubuntu/.ssh
    sudo chmod 0700 /home/ubuntu/.ssh
    echo $SSH_PUBLIC_KEY_UBUNTU | sudo tee /home/ubuntu/.ssh/authorized_keys
    sudo chmod 0600 /home/ubuntu/.ssh/authorized_keys
    sudo chown -R ubuntu:ubuntu /home/ubuntu
    "
}



init_vm () {
    echo "Initializing VM"

    filename="${GCP_SA_MANAGEMENT_CLUSTER_NAME}.json"
    encoded_sa_json=$(base64 $filename)
    gcloud compute ssh --zone $GCP_ZONE "bootstrap-vm" --project $GCP_PROJECT -- "echo '$encoded_sa_json' | base64 --decode > sa.json"


    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    if [ ! -d '/opt/sylva-core' ]; then
        sudo mkdir /opt/sylva-core
        sudo chown -R \$USER:ubuntu /opt/sylva-core
        git clone --branch $GIT_SYLVA_CORE_BRANCH $GIT_SYLVA_CORE /opt/sylva-core
        sudo apt-get update && sudo apt-get install -y make python3 curl python3-distutils python3-pip unzip docker.io git software-properties-common ca-certificates gnupg lsb-release yamllint
        sudo usermod -aG docker \$USER
        wget -qO-  https://github.com/derailed/k9s/releases/download/v0.32.3/k9s_Linux_amd64.tar.gz | tar -xz
        sudo install -o root -g root -m 0755 k9s /usr/local/bin/k9s
        sudo snap install kubectl --classic
    else
        echo 'sylva-core already cloned'
    fi"


    echo "Initialized VM"
}


generate_files () {

    export placeholder="||"
    export sa_json=$(cat "${GCP_SA_MANAGEMENT_CLUSTER_NAME}.json")
    #export sa_json=$(echo -e "${sa_json}" | sed 's/^/      /')
    export sa_json=$(echo $sa_json | sed "s/\\\\n/${placeholder}/g")
    export sa_json=$(echo $sa_json | sed 's/^/      /')
    encoded_SECRETS=$(envsubst < "./templates/secrets.yaml" | sed "s/$placeholder/\\\\n/g" | base64)
    
    encoded_KUSTOMIZATION=$(envsubst < "./templates/kustomization.yaml" | base64)
    
    encoded_VALUES=$(envsubst < "./templates/values.yaml" | base64)


    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    mkdir -p /opt/sylva-core/environment-values/current/
    echo '$encoded_KUSTOMIZATION' | base64 --decode > /opt/sylva-core/environment-values/current/kustomization.yaml
    echo '$encoded_SECRETS' | base64 --decode > /opt/sylva-core/environment-values/current/secrets.yaml
    echo '$encoded_VALUES' | base64 --decode > /opt/sylva-core/environment-values/current/values.yaml"
}

launch_bootstrap () {
    gcloud compute ssh --zone "$GCP_ZONE" "bootstrap-vm" --project "$GCP_PROJECT" --command "\
    cd /opt/sylva-core/
    ./bootstrap.sh environment-values/current/
    cp /opt/sylva-core/management-cluster-kubeconfig ~/.kube/config
    "
}

###########

# commands are applied here

###########

create_gcp_service_account
create_bootstrap_vm
wait_ssh_available
init_vm
generate_files
create_ubuntu_user_and_group
launch_bootstrap